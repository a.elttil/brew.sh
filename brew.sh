#!/bin/sh

# Install with :

which brew || ( /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" )


brew tap caskroom/versions


brew cask install google-chrome
brew install subliminal
brew cask install virtualbox
# brew cask install iterm2
brew cask install atom
brew cask install pgadmin4 
brew cask install royal-tsx
brew cask install onedrive
brew cask install firefox                                                          
brew cask install wireshark
brew cask install alfred
brew cask install appcleaner
#brew cask install backuploupe
#brew cask install beardedspice
#brew cask install cheatsheet
#brew cask install disk-inventory-x
brew cask install airfoil
brew cask install mysqlworkbench
brew cask install Franz
brew cask install intellij-idea-ce
brew cask install pycharm-ce
brew cask install flycut
brew cask install filezilla
brew cask install teamviewer
brew cask install github-desktop
#brew cask install macdown
#brew cask install macid
brew cask install spotify
#brew cask install sketch-toolbox
brew cask install vagrant
brew cask install vlc
brew cask install meld

brew install z
